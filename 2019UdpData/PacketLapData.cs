﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace _2019UdpData
{
    // Frequency: Rate as specified in menus
    // Size: 843 bytes
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PacketLapData
    {
        public PacketHeader m_header;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public LapData[] m_lapData;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct LapData
    {
        public float m_lastLapTime;        // Last lap time in seconds
        public float m_currentLapTime;
        public float m_bestLapTime;
        public float m_sector1Time;
        public float m_sector2Time;
        public float m_lapDistance;        // Distance vehicle is around the lap. negative if the line hasn't been crossed yet
        public float m_totalDistance;      // Tota distance travelled in meters. negative value possible s.o.
        public float m_safetyCarDelta;     // Delta in seconds for safety car.
        public byte m_carPosition;
        public byte m_currentLapNum;
        public byte m_pitStatus;           // 0 = none, 1 = pitting, 2 = in pit area
        public byte m_sector;              // 0 = sector 1, 1 = sector 2, 2 = sector 3
        public byte m_currentLapInvalid;   // 0 = valid, 1 = invalid
        public byte m_penalties;           // Accumulated time penalties in seconds
        public byte m_gridPosition;        // Grid position the car started in
        public byte m_driverStatus;        // 0 = in garage, 1 = flying lap, 2 = in lap, 3 = out lap, 4 = on track
        public byte m_resultStatus;        // 0 = invalid, 1 = inactive, 2 = active, 3 = finished, 4 = disqualified, 5 = not classified, 6 = retired
    }
}
