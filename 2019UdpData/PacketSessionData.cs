﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace _2019UdpData
{
    /// <summary>
    /// Frequency: 2 per Second
    /// Bytes: 149 bytes
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PacketSessionData
    {
        public PacketHeader m_header;

        public byte m_weather;

        public sbyte m_trackTemperature;
        public sbyte m_airTemperature;
        public byte m_totalLaps;
        public ushort m_trackLength;
        public byte m_sessionType;
        /**
         * 0 = unknown
         * 1 = P1
         * 2 = P2
         * 3 = P3
         * 4 = Short P
         * 5 = Q1
         * 6 = Q2
         * 7 = Q3
         * 8 = Short Q
         * 9 = One Shot Q
         * 10 = R
         * 11 = R2
         * 12 = Time Trial
         */

        public sbyte m_trckId; // -1 = Unknown
        public byte m_formula; // 0 = modern, 1 = classic, 2 = F2, 3 = Generic F1
        public ushort m_sessionTimeLeft;
        public ushort m_sessionDurtion;
        public byte m_pitSpeedLimit;
        public byte m_gamePaused;
        public byte m_isSpectation;
        public byte m_spectatorCarIndex;
        public byte m_sliProNativeSupport;
        public byte m_numMarshalZones;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 21)]
        public MarshalZone[] m_marshalZones;
        public byte m_safetyCarStatus; // 0 = no Safety Car, 1 = Real Safety Car, 2 = Virtual Safety Car
        public byte m_networkGame; // 0 = offline, 1 = online
    };

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MarshalZone
    {
        public float m_zoneStart;   // Fraction (0..1) of way through the lap the marshal zone starts
        public sbyte m_zoneFlag;     //  -1 = invalid/unknown, 0 = none, 1 = green, 2 = blue, 3 = yellow, 4 = red
    };
}
