﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace _2019UdpData
{
    /// <summary>
    /// Frequency: Every 5 Seconds
    /// Bytes: 1104 bytes
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct ParticipantsData
    {
        public byte m_aiControlled;
        public byte m_driverId;
        public byte m_teamId;
        public byte m_raceNumber;
        public byte m_nationality;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 48)]
        public char[] m_name;

        public byte m_yourTelemetry; // 0 = restricted, 1 = public
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PacketParticipantsData
    {
        public PacketHeader m_header;
        public byte m_numActiveCars;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public ParticipantsData[] m_participants;
    }
}
