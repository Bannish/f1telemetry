﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace _2019UdpData
{
    //23 byte
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PacketHeader
    {
        public ushort m_packetFormat;
        public byte m_gameMajorVersion;
        public byte m_gameMinorVersion;
        public byte m_packetVersion;
        public byte m_packetId; // see below
        public ulong m_sessionUID;
        public float m_sessionTime;
        public uint m_frameIdentifier;
        public byte m_playerCarIndex;
    }

    /**
 * ID 0 = Motion        | Motion Data for all cars
 * ID 1 = Session       | General data about the session
 * ID 2 = Lap Data      | Lap time info for all cars in the session
 * ID 3 = Event         | Session start or session end event
 * ID 4 = Participants  | list of participants in the session
 * ID 5 = Car Setups    | car setup info for cars in the race
 * ID 6 = Car Telemetry | telemetry data for all cars
 * ID 7 = Car Status    | general car status info for all cars
 */
}
