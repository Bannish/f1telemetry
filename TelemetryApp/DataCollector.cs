﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading.Tasks;



namespace TelemetryApp
{
    /// <summary>
    /// This class handles the incoming Packages and sends them over the the UI Classes
    /// </summary>
    class DataCollector
    {
        /// <summary>
        /// 
        /// </summary>
        private UdpClient client;

        /// <summary>
        /// 
        /// </summary>
        private Telemetry telemetry;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="port">Port on which to listen</param>
        /// <param name="telemetry">UI window to which the data is sent</param>
        public DataCollector(int port, Telemetry telemetry)
        {
            IPEndPoint ip = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
            client = new UdpClient(ip);
            this.telemetry = telemetry;
        }

        ~DataCollector()
        {
            log.Debug("Closing UDP Stream");
            client.Close();
        }

        /// <summary>
        /// Starts a new Task that opens a port and receives the UDP Packets from the game.
        /// </summary>
        public void StartListen()
        {
            Task.Factory.StartNew(() => Listen(client));
        }

        public static T ConvertToPacket<T>(byte[] bytes)
            where T : struct
        {
            var handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            var stuff = ConvertToPacket<T>(handle.AddrOfPinnedObject());
            handle.Free();
            return stuff;
        }

        public static T ConvertToPacket<T>(IntPtr ptr)
            where T : struct
        {
            return (T)Marshal.PtrToStructure(ptr, typeof(T));
        }

        /// <summary>
        /// Asynchroniously listens for packets from the game and sends them to the UI
        /// </summary>
        /// <param name="client">UDP Client used for listening for packets</param>
        async void Listen(UdpClient client)
        {
            log.Debug("Starting UDP Stream Listening");
            while (true)
            {
                log.Debug("----------------------------");
                try
                {
                    log.Debug("Enter Try Block");
                    var result = await client.ReceiveAsync();
                    var bytes = result.Buffer;
                    log.Debug("Bytes:" + bytes.Length);
                    UDPPacket item = ConvertToPacket<UDPPacket>(bytes);
                    telemetry.SendData(item);
                }
                catch (Exception e)
                {
                    log.Fatal(e.ToString());
                    Console.WriteLine(e.ToString());
                }
            }
        }
    }
}
