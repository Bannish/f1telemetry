﻿namespace TelemetryApp
{
    enum Tracks
    {
        Unknown = -1,
        Melbourne = 0,
        Sepang = 1,
        Shanghai = 2,
        Bahrain = 3,
        Catalunia = 4,
        Monaco = 5,
        Montreal = 6,
        Silverstone = 7,
        Hockenheim = 8,
        Hungaroring = 9,
        Spa = 10,
        Monza = 11,
        Singapore = 12,
        Suzuka = 13,
        AbuDhabi = 14,
        Texas = 15,
        Brazil = 16,
        Austria = 17,
        Sochi = 18,
        Mexico = 19,
        Baku = 20,
        BahrainShort = 21,
        SilverstoneShort = 22,
        TexasShort = 23,
        SuzukaShort = 24,
    }
}
