﻿using System.Windows.Media;

namespace TelemetryApp
{
    class Constants
    {
        // Tyre Colors
        public static readonly Brush HYPERSOFTCOLOR = new SolidColorBrush(Color.FromRgb(237, 170, 183));
        public static readonly Brush ULTRASOFTCOLOR = new SolidColorBrush(Color.FromRgb(218, 87, 184));
        public static readonly Brush SUPERSOFTCOLOR = new SolidColorBrush(Color.FromRgb(255, 68, 39));
        public static readonly Brush SOFTCOLOR = new SolidColorBrush(Color.FromRgb(229, 208, 6));
        public static readonly Brush MEDIUMCOLOR = new SolidColorBrush(Color.FromRgb(255, 255, 255));
        public static readonly Brush HARDCOLOR =  new SolidColorBrush(Color.FromRgb(48,202,255));
        public static readonly Brush SUPERHARDCOLOR = new SolidColorBrush(Color.FromRgb(255, 143, 19));
        public static readonly Brush INTERSCOLOR = new SolidColorBrush(Color.FromRgb(113, 229, 105));
        public static readonly Brush WETSCOLOR = new SolidColorBrush(Color.FromRgb(111, 180, 237));

        // Team Colors
        public static readonly Brush CLASSICTEAMS = new SolidColorBrush(Color.FromRgb(255, 255, 255));
        public static readonly Brush DEFAULTTEAM = new SolidColorBrush(Color.FromRgb(17, 17, 25));
        public static readonly Brush REDBULL = new SolidColorBrush(Color.FromRgb(40, 57, 183));
        public static readonly Brush FERRARI = new SolidColorBrush(Color.FromRgb(158, 17, 17));
        public static readonly Brush MCLAREN = new SolidColorBrush(Color.FromRgb(217, 132, 17));
        public static readonly Brush RENAULT = new SolidColorBrush(Color.FromRgb(243, 241, 50));
        public static readonly Brush MERCEDES = new SolidColorBrush(Color.FromRgb(34, 201, 182));
        public static readonly Brush SAUBER = new SolidColorBrush(Color.FromRgb(112, 15, 40));
        public static readonly Brush FORCEINDIA = new SolidColorBrush(Color.FromRgb(196, 152, 150));
        public static readonly Brush WILLIAMS = new SolidColorBrush(Color.FromRgb(222, 220, 228));
        public static readonly Brush TORROROSSO = new SolidColorBrush(Color.FromRgb(72, 131, 192));
        public static readonly Brush HAAS = new SolidColorBrush(Color.FromRgb(123, 124, 113));


    }
}
