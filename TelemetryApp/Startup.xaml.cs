﻿using System;
using System.Windows;

namespace TelemetryApp
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class Startup : Window
    {

        private Properties.Settings settings = Properties.Settings.Default;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Startup()
        {
            InitializeComponent();
            if (System.Threading.Thread.CurrentThread.CurrentUICulture.Name != "de-DE")
            {
                BtnEng.IsChecked = true;
                BtnGer.IsChecked = false;
            }
            else if (System.Threading.Thread.CurrentThread.CurrentUICulture.Name != "en-US")
            {
                BtnGer.IsChecked = true;
                BtnEng.IsChecked = false;
            }
            ListenPort.Text = "" + settings.Port;
            Abbr.Text = settings.Abbr;
        }

        public Startup(string abbr = "", string port = "20777")
        {
            InitializeComponent();
            ListenPort.Text = "" + port;
            Abbr.Text = abbr;
            if (System.Threading.Thread.CurrentThread.CurrentUICulture.Name != "de-DE")
            {
                BtnEng.IsChecked = true;
                BtnGer.IsChecked = false;
            }
            else if (System.Threading.Thread.CurrentThread.CurrentUICulture.Name != "en-US")
            {
                BtnGer.IsChecked = true;
                BtnEng.IsChecked = false;
            }
        }

        private void BtnGer_Click(object sender, RoutedEventArgs e)
        {
            if (System.Threading.Thread.CurrentThread.CurrentUICulture.Name != "de-DE")
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture =
            new System.Globalization.CultureInfo("de-DE");
                Refresh();
            }
        }

        private void BtnEng_Click(object sender, RoutedEventArgs e)
        {
            if(System.Threading.Thread.CurrentThread.CurrentUICulture.Name != "en-US")
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture =
            new System.Globalization.CultureInfo("en-US");
                Refresh();
            }
        }

        private void StartListen_Click(object sender, RoutedEventArgs e)
        {
            int port = Int32.Parse(ListenPort.Text);
            settings.Port = port;
            settings.Abbr = Abbr.Text.ToUpper();
            log.Info("Saving Port" + port + " and Abbreviation " + Abbr.Text.ToUpper());
            settings.Save();
            log.Debug("Opening Telemetry Window with Port "+ port + " and Abbreviation " + Abbr.Text.ToUpper());
            Window telemetry = new Telemetry(port, Abbr.Text.ToUpper());
            telemetry.Show();
            Close();
        }


        public void Refresh()
        {
            Window child = new Startup(Abbr.Text.ToUpper(), ListenPort.Text);
            Close();
            child.Show();
        }
    }
}
