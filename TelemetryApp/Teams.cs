﻿namespace TelemetryApp
{
    enum Teams
    {
        RedBull = 0,
        Ferrari = 1,
        McLaren = 2,
        Renault = 3,
        Mercedes = 4,
        Sauber = 5,
        ForceIndia = 6,
        Williams = 7,
        ToroRosso = 8,
        Haas = 11,
    }
    enum ClassicTeams
    {
        Williams1992 = 0,
        McLaren1988 = 1,
        McLaren2008 = 2,
        Ferrari2004 = 3,
        Ferrari1995 = 4,
        Ferrari2007 = 5,
        McLaren1998 = 6,
        Williams1996 = 7,
        Renault2006 = 8,
        Ferrari2002 = 10,
        RedBull2010 = 11,
        McLaren1991 = 12,
    }
}
