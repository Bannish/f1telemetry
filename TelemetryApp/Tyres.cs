﻿namespace TelemetryApp
{
    enum Tyres
    {
        HYPERSOFT = -1,
        ULTRASOFT = 0,
        SUPERSOFT = 1,
        SOFT = 2,
        MEDIUM = 3,
        HARD = 4,
        SUPERHARD = 7,
        INTERS = 5,
        WETS = 6,

    }
}
