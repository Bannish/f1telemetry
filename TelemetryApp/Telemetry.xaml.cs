﻿using TelemetryCustomControls;
using System;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace TelemetryApp
{
    /// <summary>
    /// Interaktionslogik für Telemetry.xaml
    /// </summary>
    public partial class Telemetry : Window
    {
        private DataCollector Collector;

        private static UDPPacket Packet;
        private static byte CarIndex;
        private static CarUDPData PlayerCar;
        private string Abbr = "";
        private float Lap = -1.0f;
        private float Sector = 0.0f;
        private float Flag = 0.0f;
        private float LapTime = 0.0f;
        private float Sector1 = 0.0f;
        private float Sector2 = 0.0f;
        private float Sector3 = 0.0f;
        private byte BestLapIndex = 0;
        private byte BestSector1Index = 0;
        private byte BestSector2Index = 0;
        private byte BestSector3Index = 0;
        private TimingEntry[] Timings;
        private LapTimeEntry ThisLap;
        private LapTimeEntry[] LapTimes;
        private Tracks CurrentTrack;
        private float FuelStand = 0;
        private bool Running = false;
        private static AutoResetEvent lapCounterReset;
        private static AutoResetEvent lapTimesReset;
        private static AutoResetEvent leaderBoardReset;
        private static AutoResetEvent carDataReset;
        private float[,] TimeDeltaFirst;
        Thread LapCounterThread;
        Thread TimingListThread;
        Thread LapTimesThread;
        Thread CarDataElementsThread;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public Telemetry(int listenPort, string abbr = "")
        {
            TimeDeltaFirst = new float[byte.MaxValue, short.MaxValue];
            InitializeComponent();
            Abbr = abbr;
            lapCounterReset = new AutoResetEvent(false);
            leaderBoardReset = new AutoResetEvent(false);
            lapTimesReset = new AutoResetEvent(false);
            carDataReset = new AutoResetEvent(false);
            Collector = new DataCollector(listenPort, this);
            Collector.StartListen();
            bestLap.UpdateEntry(Properties.Resources.strBest);
            fuelPerLap.Content = Properties.Resources.strFuelPerLap + ": " + 0.00;
            Timings = new TimingEntry[20];
            LapTimes = new LapTimeEntry[99];
            ThisLap = new LapTimeEntry()
            {
                Lap = 1,
            };
            timingPanel.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() =>
            {
                for (int i = 0; i < Timings.Length; i++)
                {
                    var tmp = new TimingEntry(i + 1)
                    {
                        HorizontalAlignment = HorizontalAlignment.Left
                    };
                    Timings[i] = tmp;
                    timingPanel.Children.Add(tmp);
                }
            }));
        }

        ~Telemetry() {
            Running = false;
            lapTimesReset.Set();
            lapCounterReset.Set();
            leaderBoardReset.Set();
            carDataReset.Set();
            if (LapCounterThread != null)
            {
                LapCounterThread.Abort();
                TimingListThread.Abort();
                LapTimesThread.Abort();
                CarDataElementsThread.Abort();
            }
            
        }

        internal void SendData(UDPPacket pckt)
        {
            log.Debug("Packet Received");
            Packet = pckt;
            CarIndex = pckt.PlayerCarIndex;
            PlayerCar = pckt.CarData[CarIndex];
            if(CurrentTrack != (Tracks)pckt.TrackNumber)
            {
                ResetValues();
                CurrentTrack = (Tracks)pckt.TrackNumber;
                Lap = 0;   
            }

            if (Sector != Packet.Sector)
            {
                Sector = Packet.Sector;
                carDataReset.Set();
                GetSectorTimes();
            }

            if (Lap != Packet.Lap)
            {
                Lap = Packet.Lap;
                lapCounterReset.Set();
                lapTimesReset.Set();
            }


            if(Packet.Time == 0)
            {
                FuelStand = Packet.FuelInTank;
            }

            if (!Running)
            {
                Running = true;
                FuelStand = Packet.FuelInTank;
                SetupThreads();
            }
            else
            {
                leaderBoardReset.Set();
            }
            
        }

        private void SetupThreads()
        {
            log.Debug("Setting up threads");
            LapCounterThread = new Thread(new ThreadStart(UpdateLaps)) { Name = "Lap Counter Thread" };
            LapCounterThread.IsBackground = true;
            TimingListThread = new Thread(new ThreadStart(UpdateTimingsList)) { Name = "Timing List Thread" };
            TimingListThread.IsBackground = true;
            LapTimesThread = new Thread(new ThreadStart(UpdateLapTimes)) { Name = "Lap Times Thread" };
            LapTimesThread.IsBackground = true;
            CarDataElementsThread = new Thread(new ThreadStart(UpdateCarData)) { Name = "Car Data View Thread" };
            CarDataElementsThread.IsBackground = true;
            LapCounterThread.Start();
            TimingListThread.Start();
            LapTimesThread.Start();
            CarDataElementsThread.Start();
            log.Debug("Threads started");
        }

        private void UpdateFuelPerLap()
        {
            carDataView.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() =>
            {
                double fpp = Math.Round((FuelStand - Packet.FuelInTank) / Lap, 3);
                fuelPerLap.Content = Properties.Resources.strFuelPerLap + ": " + fpp;
            }));
        }

        private void UpdateCarData()
        {
            while (Running)
            {
                carDataReset.WaitOne();
                carDataView.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() =>
                {
                    
                    fuelLevel.Maximum = Packet.FuelCapacity;
                    fuelLevel.Value = Packet.FuelInTank;
                    fuelLevelNum.Content = Math.Round(Packet.FuelInTank, 3);
                }));
            }
        }

        private void UpdateLapTimes()
        {
            while(Running)
            {
                lapTimesReset.WaitOne();
                log.Info("Updating Lap Times List");
                lapTimesView.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() =>
                {
                    ThisLap.LapTime = LapTime;
                    ThisLap.Tyre = (Tyres)PlayerCar.TyreCompound;
                }));
            }
        }

        private void UpdateTimingsList()
        {
            while (Running)
            {
                leaderBoardReset.WaitOne();
                log.Info("Updating Timings List");
                GetFirstCarTimeDelta();
                timingPanel.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() =>
                {
                    
                    CarUDPData[] cars = Packet.CarData;
                    for (int i = 0; i < Packet.NumCars; i++)
                    {
                        string abr = "";
                        Brush teamColor = GetTeamColor(cars[i].TeamId, Packet.Era);
                        if (i == CarIndex)
                        {
                            if(Abbr != "")
                            {
                                abr = Abbr;
                                Timings[cars[i].CarPosition - 1].UpdateEntry(teamColor, abr, "---", GetTyreType(cars[i]));
                            } else
                            {
                                abr = GetAbbreviation(cars[i].DriverId, Packet.Era);
                                Timings[cars[i].CarPosition - 1].UpdateEntry(teamColor, abr, "---", GetTyreType(cars[i]));
                            }    
                        }
                        else
                        {
                            abr = GetAbbreviation(cars[i].DriverId, Packet.Era);
                            Timings[cars[i].CarPosition - 1].UpdateEntry(teamColor, abr, GetDeltaTime(cars[i], Packet.SessionTimeLeft), GetTyreType(cars[i]));
                        }

                    }
                }));
            }
        }

        private void GetFirstCarTimeDelta()
        {
            log.Debug("Get Delta");
            for(int i = 0; i < Packet.NumCars; i++)
            {
                log.Debug("Find Driver in First place");
                if (Packet.CarData[i].CarPosition == 1)
                {
                    log.Debug("Found Driver in first place " + GetAbbreviation(Packet.CarData[i].DriverId, Packet.Era));
                    TimeDeltaFirst[Packet.CarData[i].CurrentLapNum, (int)(Math.Round(Packet.CarData[i].LapDistance, 0) / 10)] = Packet.SessionTimeLeft;
                    break;
                }
            } 
        }

        private Brush GetTyreType(CarUDPData carUDPData)
        {
            switch((Tyres)carUDPData.TyreCompound)
            {
                case Tyres.ULTRASOFT: return Constants.ULTRASOFTCOLOR;
                case Tyres.SUPERSOFT: return Constants.SUPERSOFTCOLOR;
                case Tyres.SOFT: return Constants.SOFTCOLOR;
                case Tyres.MEDIUM: return Constants.MEDIUMCOLOR;
                    //Hardcolor has to be changed for F1 2018
                case Tyres.HARD: return Constants.HARDCOLOR;
                case Tyres.INTERS: return Constants.INTERSCOLOR;
                case Tyres.WETS: return Constants.WETSCOLOR;
                default: return Constants.HYPERSOFTCOLOR;
            }
        }

        private string GetDeltaTime(CarUDPData rival, float sessionTime)
        {
            log.Debug("Calculating Delta to " + GetAbbreviation(rival.DriverId,Packet.Era));

            float gapDriver = 0;
            string strDelta = "";

            if (rival.CarPosition == 1)
            {
                gapDriver = Math.Abs(TimeDeltaFirst[PlayerCar.CurrentLapNum, (int)(Math.Round(PlayerCar.LapDistance, 0) / 10)] - sessionTime);
            }
            else
            {
                float gapPlayer = Math.Abs(TimeDeltaFirst[PlayerCar.CurrentLapNum, (int)(Math.Round(PlayerCar.LapDistance, 0) / 10)] - sessionTime);
                if(PlayerCar.CarPosition == 1)
                {
                    gapDriver = Math.Abs(TimeDeltaFirst[rival.CurrentLapNum, (int)(Math.Round(rival.LapDistance, 0) / 10)] - sessionTime);
                }
                else if (rival.CarPosition < PlayerCar.CarPosition)
                {
                    gapDriver = Math.Abs(TimeDeltaFirst[rival.CurrentLapNum, (int)(Math.Round(rival.LapDistance, 0) / 10)] - sessionTime);
                    gapDriver = gapPlayer - gapDriver;
                }
                else
                {
                    gapDriver = Math.Abs(TimeDeltaFirst[rival.CurrentLapNum, (int)(Math.Round(rival.LapDistance, 0) / 10)] - sessionTime);
                    gapDriver = gapDriver - gapPlayer;
                }
            }
            
            strDelta = TimeSpan.FromSeconds(gapDriver).ToString("mm'.'ss'.'fff");
            log.Debug("Delta to " + GetAbbreviation(rival.DriverId, Packet.Era) + " is " + strDelta);
            return strDelta;
        }

        private Brush GetTeamColor(int teamId, float era = 2017)
        {
            if (era == 2017)
            {
                switch ((Teams)teamId)
                {
                    case Teams.RedBull:     return Constants.REDBULL;
                    case Teams.Ferrari:     return Constants.FERRARI;
                    case Teams.McLaren:     return Constants.MCLAREN;
                    case Teams.Renault:     return Constants.RENAULT;
                    case Teams.Mercedes:    return Constants.MERCEDES;
                    case Teams.Sauber:      return Constants.SAUBER;
                    case Teams.ForceIndia:  return Constants.FORCEINDIA;
                    case Teams.Williams:    return Constants.WILLIAMS;
                    case Teams.ToroRosso:   return Constants.TORROROSSO;
                    case Teams.Haas:        return Constants.HAAS;
                    default:                return Constants.DEFAULTTEAM;
                }
            }
            else
            {
                switch ((ClassicTeams)teamId)
                {
                    case ClassicTeams.Williams1992: return Constants.WILLIAMS;
                    case ClassicTeams.McLaren1988:  return Constants.MCLAREN;
                    case ClassicTeams.McLaren2008:  return Constants.MCLAREN;
                    case ClassicTeams.Ferrari2004:  return Constants.FERRARI;
                    case ClassicTeams.Ferrari1995:  return Constants.FERRARI;
                    case ClassicTeams.Ferrari2007:  return Constants.FERRARI;
                    case ClassicTeams.McLaren1998:  return Constants.MCLAREN;
                    case ClassicTeams.Renault2006:  return Constants.RENAULT;
                    case ClassicTeams.Ferrari2002:  return Constants.FERRARI;
                    case ClassicTeams.RedBull2010:  return Constants.REDBULL;
                    case ClassicTeams.McLaren1991:  return Constants.MCLAREN;
                    default:                        return Constants.CLASSICTEAMS;
                }
            }
            
        }

        private string GetAbbreviation(int driverID, float era = 2017)
        {
            if(era == 2017)
            {
                switch ((Drivers)driverID)
                {
                    case Drivers.VET: return Drivers.VET.ToString();
                    case Drivers.KVY: return Drivers.KVY.ToString();
                    case Drivers.ALO: return Drivers.ALO.ToString();
                    case Drivers.MAS: return Drivers.MAS.ToString();
                    case Drivers.PER: return Drivers.PER.ToString();
                    case Drivers.RAI: return Drivers.RAI.ToString();
                    case Drivers.GRO: return Drivers.GRO.ToString();
                    case Drivers.HAM: return Drivers.HAM.ToString();
                    case Drivers.HUL: return Drivers.HUL.ToString();
                    case Drivers.MAG: return Drivers.MAG.ToString();
                    case Drivers.BOT: return Drivers.BOT.ToString();
                    case Drivers.RIC: return Drivers.RIC.ToString();
                    case Drivers.ERI: return Drivers.ERI.ToString();
                    case Drivers.PAL: return Drivers.PAL.ToString();
                    case Drivers.VER: return Drivers.VER.ToString();
                    case Drivers.SAI: return Drivers.SAI.ToString();
                    case Drivers.WEH: return Drivers.WEH.ToString();
                    case Drivers.OCO: return Drivers.OCO.ToString();
                    case Drivers.VAN: return Drivers.VAN.ToString();
                    case Drivers.STR: return Drivers.STR.ToString();
                    default: return "";
                }
            } else
            {
                switch ((ClassicDrivers)driverID)
                {
                    case ClassicDrivers.MIC: return ClassicDrivers.MIC.ToString();
                    case ClassicDrivers.GIL: return ClassicDrivers.GIL.ToString();
                    case ClassicDrivers.COR: return ClassicDrivers.COR.ToString();
                    case ClassicDrivers.LEV: return ClassicDrivers.LEV.ToString();
                    case ClassicDrivers.FOR: return ClassicDrivers.FOR.ToString();
                    case ClassicDrivers.MOR: return ClassicDrivers.MOR.ToString();
                    case ClassicDrivers.SAA: return ClassicDrivers.SAA.ToString();
                    case ClassicDrivers.BEL: return ClassicDrivers.BEL.ToString();
                    case ClassicDrivers.KAU: return ClassicDrivers.KAU.ToString();
                    case ClassicDrivers.ATI: return ClassicDrivers.ATI.ToString();
                    case ClassicDrivers.CLA: return ClassicDrivers.CLA.ToString();
                    case ClassicDrivers.LAU: return ClassicDrivers.LAU.ToString();
                    case ClassicDrivers.COP: return ClassicDrivers.COP.ToString();
                    case ClassicDrivers.MUR: return ClassicDrivers.MUR.ToString();
                    case ClassicDrivers.CAL: return ClassicDrivers.CAL.ToString();
                    case ClassicDrivers.LET: return ClassicDrivers.LET.ToString();
                    case ClassicDrivers.IZU: return ClassicDrivers.IZU.ToString();
                    case ClassicDrivers.BAR: return ClassicDrivers.BAR.ToString();
                    case ClassicDrivers.JSC: return ClassicDrivers.JSC.ToString();
                    case ClassicDrivers.FNI: return ClassicDrivers.FNI.ToString();
                    case ClassicDrivers.VIS: return ClassicDrivers.VIS.ToString();
                    case ClassicDrivers.WAL: return ClassicDrivers.WAL.ToString();
                    case ClassicDrivers.QES: return ClassicDrivers.QES.ToString();
                    case ClassicDrivers.ROT: return ClassicDrivers.ROT.ToString();
                    default: return "";
                }
            }
            
        }

        private void UpdateLaps()
        {
            while(Running)
            {
                lapCounterReset.WaitOne();
                log.Debug("Updating number of Laps");
                Laps.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() =>
                {
                    raceTrack.Content = CurrentTrack.ToString();
                    laps.Content = "" + (Lap + 1) + "/" + Packet.TotalLaps;
                    laps.Foreground = Brushes.White;
                    LapString.Content = Properties.Resources.strLaps;
                    LapString.Foreground = Brushes.White;
                }));
                UpdateFuelPerLap();
                BestLapUpdated();
            }
        }

        private string GetValidString(byte valid)
        {
            if (valid == 0)
            {
                return Properties.Resources.strValid;
            }
            else
            {
                return Properties.Resources.strInvalid;
            }
        }

        private void BestLapUpdated()
        {
            log.Debug("Updating Best Lap");
            LapTime = PlayerCar.LastLapTime;
            if (PlayerCar.LastLapTime == PlayerCar.BestLapTime)
            {
                bestLap.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => {
                    bestLap.UpdateEntry(Properties.Resources.strBest, TimeSpan.FromSeconds(Sector1).ToString("ss'.'fff"), TimeSpan.FromSeconds(Sector2).ToString("ss'.'fff"), TimeSpan.FromSeconds(Sector3).ToString("ss'.'fff"), TimeSpan.FromSeconds(PlayerCar.LastLapTime).ToString("mm':'ss'.'fff"), "", GetValidString(PlayerCar.CurrentLapInvalid), ((Tyres)PlayerCar.TyreCompound).ToString());
                }));
            }
        }

        private void GetSectorTimes()
        {
            switch (Sector)
            {
                case 0.0f:
                    Sector1 = Packet.Sector1Time;
                    ThisLap.Sector1 = Sector1;
                    break;
                case 1.0f:
                    Sector2 = Packet.Sector2Time;
                    ThisLap.Sector2 = Sector2;
                    break;
                default:
                    Sector3 = Packet.LastLapTime - Sector1 - Sector2;
                    ThisLap.Sector3 = Sector3;
                    break;
            }
        }

        private void ResetValues()
        {
            log.Debug("Resetting Values");
            TimeDeltaFirst = new float[byte.MaxValue, short.MaxValue];
            Timings = new TimingEntry[20];
            LapTimes = new LapTimeEntry[(int)Packet.TotalLaps];
            timingPanel.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() =>
            {
                raceTrack.Content = CurrentTrack.ToString();
                fuelPerLap.Content = 0;
                log.Debug("Reset Best Lap");
                bestLap.UpdateEntry(Properties.Resources.strBest);
                log.Debug("Reset Lap Times");
                lapTimesView.Children.Clear();
                log.Debug("Create new leaderboard");
                timingPanel.Children.Clear();
                for (int i = 0; i < Timings.Length; i++)
                {
                    var tmp = new TimingEntry(i + 1)
                    {
                        HorizontalAlignment = HorizontalAlignment.Left
                    };
                    Timings[i] = tmp;
                    timingPanel.Children.Add(tmp);
                }
            }));
            Lap = 0;
            LapTime = 0;
            Sector = 0;
            Sector1 = 0;
            Sector2 = 0;
            Sector3 = 0;
            BestLapIndex = 0;
            BestSector1Index = 0;
            BestSector2Index = 0;
            BestSector3Index = 0;
        }
    }
}
