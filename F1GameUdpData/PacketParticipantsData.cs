﻿using System.Runtime.InteropServices;

namespace _2018UdpData
{
    /// <summary>
    /// Frequency: Every 5 Seconds
    /// Bytes: 1082 bytes
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PacketParticipantsData
    {
        public PacketHeader m_header;

        public byte m_numCars;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public ParticipantData[] m_participants;
    };

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct ParticipantData
    {
        public byte m_aiControlled; // Human = 0, AI = 1
        public byte m_driverId;
        public byte m_teamId;
        public byte m_raceNumber;
        public byte m_nationality;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 48)]
        public char[] m_name;
    };
}
