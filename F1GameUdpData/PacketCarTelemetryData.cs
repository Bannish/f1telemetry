﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace _2018UdpData
{
    /// <summary>
    /// Frequency: Specified in Menu
    /// Bytes: 1085 bytes
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PacketCarTelemetryData
    {
        public PacketHeader m_header;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public CarTelemetryData[] m_carTelemetryData;

        public uint m_buttonStatus; //Bit Flags
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CarTelemetryData
    {
        public ushort m_speed;
        public byte m_throttle; //0..100
        public sbyte m_steer;  //-100 .. 100
        public byte m_break;   //0..100
        public byte m_clutch; //0..100
        public sbyte m_gear; // N = 0, R = -1, D = 1-8
        public ushort m_engineRPM;
        public byte m_drs; // 0 = off, 1 = on
        public byte m_revLightsPercent; //Rev lights indicator (percent)

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public ushort[] m_breaksTemperature; // In Celsius

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public ushort[] m_tyresSurfaceTemperature; // Tyres surface temp (celsius)

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public ushort[] m_tyreInnerTemperature; // In Celsius

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public ushort m_engineTemperature; // In celsius

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public float[] m_tyrePressure; // in PSI
    }
}
