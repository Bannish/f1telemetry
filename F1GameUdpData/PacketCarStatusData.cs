﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace _2018UdpData
{
    /// <summary>
    /// Frequency: 2 per second
    /// Bytes: 1061 bytes
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PacketCarStatusData
    {
        public PacketHeader m_header;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public CarStatusData[] m_carStatusData;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CarStatusData
    {
        public byte m_tractionControl; // 0 (off) - 2 (high)
        public byte m_antiLockBrakes; // 0 (off) - 1 (on)
        public byte m_fuelMix; // 0 = lean, 1 = standard, 2 = rich, 3 = max
        public byte m_frontBrakesBias; // percent
        public byte m_pitLimiterStatus; // 0 = off, 1 = on
        public float m_fuelInTank; //current fuel mass
        public float m_fuelCapacity;
        public ushort m_maxRPM;
        public ushort m_idleRPM;
        public byte m_maxGears;
        public byte m_drsAllowed; // 0 = not allowed, 1 = allowed, -1 = unknown
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] m_tyresWear; // percent
        public byte m_tyreCompound; // Modern: 0 = HS, 1 = US, 2 = SS, 3 = S, 4 = M, 5 = H, 6 = SH, 7 = I, 8 = W
                                    // Classic: 0 - 6 = dry, 7-8 = wet
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] m_tyresDamage; // percentage
        public byte m_frontLeftWingDamage; // percentage
        public byte m_frontRightWingDamage; // percentage
        public byte m_rearWingDamage; // percentage
        public byte m_engineDamage; // percentage
        public byte m_gearBoxDamage; // percentage
        public byte m_exhaustDamage; // percentage
        public sbyte m_vehicleFiaFlags; // -1 = invalid/unknown, 0 = none, 1 = green, 2 = blue, 3 = yellow, 4 = red
        public float m_ersStoreEnergy; // ERS Energy store in Joules
        public byte m_ersDeployMode; // 0 = none, 1 = low, 2 = medium, 3 = high, 4 = overtake, 5 = hotlap
        public float m_ersHarvestedThisLapMGUK; // harvested by MGU-K
        public float m_ersHarvestedThisLapMGUH; // harvested by MGU-H
        public float m_ersDeployedThisLap;
    }
}
