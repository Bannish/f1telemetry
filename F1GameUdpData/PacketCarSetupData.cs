﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace _2018UdpData
{

    /// <summary>
    /// Frequency: Every 5 Seconds
    /// Bytes: 841 bytes
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PacketCarSetupData
    {
        public PacketHeader m_header;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public CarSetupData[] m_carSetupData;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CarSetupData
    {
        public byte m_frontWing;
        public byte m_rearWing;
        public byte m_onThrottle; //percentage
        public byte m_offThrottle; //percentage
        public float m_frontCamber;
        public float m_rearCamber;
        public float m_fronToe;
        public float m_rearTo;
        public byte m_frontSuspension;
        public byte m_rearSuspension;
        public byte m_frontAntiRollBar;
        public byte m_rearAntiRollBar;
        public byte m_frontSuspensionHeight;
        public byte m_rearSuspensionHeight;
        public byte m_breakPressure; //percentage
        public byte m_breakBias; //percentage
        public float m_frontTyrePressure; //PSI
        public float m_rearTyrePressure; //PSI
        public byte m_ballast;
        public float m_fuelLoad;
    }
}
