﻿using System;
using System.Runtime.InteropServices;

namespace _2018UdpData
{
    /// <summary>
    /// Frequency: Specified in Menu
    /// Bytes: 1341 bytes
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PacketMotionData
    {
        public PacketHeader m_header;  //Header

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public CarMotionData[] m_carMotionData;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)] // Wheel arrays: RL, RR, FL, FR
        public float[] m_suspensionPosition;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public float[] m_suspensionVelocity;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public float[] m_suspensionAcceleration;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public float[] m_wheelSpeed;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public float[] m_wheelSlip;

        public float m_localVelocityX;
        public float m_localVelocityY;
        public float m_localVelocityZ;
        public float m_angularVelocityX;
        public float m_angularVelocityY;
        public float m_angularVelocityZ;
        public float m_angularAccelerationX;
        public float m_angularAccelerationY;
        public float m_angularAccelerationZ;
        public float m_frontWheelAngle;
    }

    
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CarMotionData
    {
        public float m_worldPositionX;
        public float m_worldPositionY;
        public float m_worldPositionZ;
        public float m_worldVelocityX;
        public float m_worldVelocityY;
        public float m_worldVelocityZ;
        public short m_worldForwardDirX;
        public short m_worldForwardDirY;
        public short m_worldForwardDirZ;
        public short m_worldRightDirX;
        public short m_worldRightDirY;
        public short m_worldRightDirZ;
        public float m_gForceLateral;
        public float m_gForceLongitudinal;
        public float m_gForceVertical;
        public float m_yaw;
        public float m_pitch;
        public float m_roll;
    }
}
