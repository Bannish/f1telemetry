﻿using System;
using System.Runtime.InteropServices;

namespace _2018UdpData
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PacketHeader
    {
        public ushort m_packetFormat; //2018

        public byte m_packetVersion; //Version of this packet type, all start from 1

        public byte m_packetId; //Identifier for this packet type, see below

        public ulong m_sessionUID; //Unique identifier for the session

        public float m_sessionTime; //Session timestamp

        public uint m_frameIdentifier; //Identifier for the frame the data was retrieved on

        public byte m_playerCarIndex; //Index of player's car in the array
    }

    /**
     * ID 0 = Motion        | Motion Data for all cars
     * ID 1 = Session       | General data about the session
     * ID 2 = Lap Data      | Lap time info for all cars in the session
     * ID 3 = Event         | Session start or session end event
     * ID 4 = Participants  | list of participants in the session
     * ID 5 = Car Setups    | car setup info for cars in the race
     * ID 6 = Car Telemetry | telemetry data for all cars
     * ID 7 = Car Status    | general car status info for all cars
     */


}
