﻿using System.Runtime.InteropServices;

namespace _2018UdpData
{

    /// <summary>
    /// Frequency: On Event
    /// Bytes: 25 bytes
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PacketEventData
    {
        public PacketHeader m_header;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] m_eventStringCode; // "SSTA" Session starts, "SEND" Session Ends
    }
}
