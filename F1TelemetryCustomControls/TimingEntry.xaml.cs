﻿using System.Windows.Controls;
using System.Windows.Media;

namespace TelemetryCustomControls
{
    /// <summary>
    /// Interaktionslogik für F1TimingEntry.xaml
    /// </summary>
    public partial class TimingEntry : UserControl
    {

        /// <summary>
        /// Position Number Property
        /// </summary>
        public object PositionNumber
        {
            get { return positionNumber.Content; }
            private set { positionNumber.Content = value; }
        }

        /// <summary>
        /// Abbreviation Property
        /// </summary>
        public object Abbreviation
        {
            get { return abbreviation.Content; }
            private set { abbreviation.Content = value; }
        }

        /// <summary>
        /// Timing Interval Property
        /// </summary>
        public object Interval
        {
            get { return interval.Content; }
            private set { interval.Content = value; }
        }

        /// <summary>
        /// Team Color Property
        /// </summary>
        public Brush TeamColor
        {
            get { return teamColor.Fill; }
            private set { teamColor.Fill = value; }
        }

        public Brush Tyre
        {
            get { return tyre.Background; }
            private set { tyre.Background = value; }
        }

        public object Penalty
        {
            get { return penalty.Content; }
            private set { penalty.Content = value; }
        }

        public TimingEntry()
        {
            InitializeComponent();
            PositionNumber = 1;
        }

        /// <summary>
        /// Creates a new timing Entry
        /// </summary>
        /// <param name="pos">Position Number</param>
        public TimingEntry(int pos)
        {
            InitializeComponent();
            PositionNumber = pos;
        }

        /// <summary>
        /// Updates the Timing Entry
        /// </summary>
        /// <param name="tCol">Team Color</param>
        /// <param name="abr">Abbreviation</param>
        /// <param name="timing">Timing Interval</param>
        public void UpdateEntry(Brush tCol, string abr, string timing, Brush tyre, string penal = "")
        {
            TeamColor = tCol;
            Abbreviation = abr;
            Interval = timing;
            Tyre = tyre;
            Penalty = penal;
        }
    }
}
