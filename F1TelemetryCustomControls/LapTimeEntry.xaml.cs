﻿using System.Windows.Controls;
using System.Windows.Media;

namespace TelemetryCustomControls
{
    /// <summary>
    /// Interaktionslogik für LapTimeEntry.xaml
    /// </summary>
    public partial class LapTimeEntry : UserControl
    {

        public object Lap
        {
            get { return lap.Content; }
            set { lap.Content = value; }
        }
        public object Sector1
        {
            get { return sector1.Content; }
            set { sector1.Content = value; }
        }
        public object Sector2
        {
            get { return sector2.Content; }
            set { sector2.Content = value; }
        }
        public object Sector3
        {
            get { return sector3.Content; }
            set { sector3.Content = value; }
        }
        public object LapTime
        {
            get { return lapTime.Content; }
            set { lapTime.Content = value; }
        }
        public object Valid
        {
            get { return valid.Content; }
            set { valid.Content = value; }
        }
        public object Delta
        {
            get { return delta.Content; }
            set { delta.Content = value; }
        }
        public object Tyre
        {
            get { return tyre.Content; }
            set { tyre.Content = value; }
        }

        public LapTimeEntry()
        {
            InitializeComponent();
            Lap = 1;
        }
        public LapTimeEntry(int lap)
        {
            InitializeComponent();
            Lap = lap;
        }

        public void UpdateEntry(string lap ="",string sector1="", string sector2="", string sector3="", string lapTime="", string delta="", string valid ="", string tyre="")
        {
            Lap = lap;
            Sector1 = sector1;
            Sector2 = sector2;
            Sector3 = sector3;
            LapTime = lapTime;
            Delta = delta;
            Valid = valid;
            Tyre = tyre;
        }

        public void UpdateSector(int sector, string time, Brush color)
        {
            if(sector == 0)
            {
                Sector1 = time;
                sector1.Foreground = color;
            } else if (sector == 1)
            {
                Sector2 = time;
                sector2.Foreground = color;
            } else if (sector == 2)
            {
                Sector3 = time;
                sector3.Foreground = color;
            }
        } 

        public void UpdateLapTime(string time, string valid, Brush color)
        {
            LapTime = time;
            Valid = valid;
            lapTime.Foreground = color;
        }
    }
}
