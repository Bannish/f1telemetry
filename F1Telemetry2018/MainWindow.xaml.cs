﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Windows;

namespace F1Telemetry
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int port = 20777;
        DataCollector collector;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public MainWindow()
        {
            InitializeComponent();
            Console.WriteLine("Start Server");

            collector = new DataCollector(port);
            log.Debug("Start Server");
            collector.StartListen();
        }

        ~MainWindow()
        {
            log.Debug("MainWindow Destroyer");

        }
    }
}
