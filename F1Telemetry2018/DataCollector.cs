﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using _2019UdpData;



namespace F1Telemetry
{
    /// <summary>
    /// This class handles the incoming Packages and sends them over the the UI Classes
    /// </summary>
    public class DataCollector
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly UdpClient client;

        /// <summary>
        /// 
        /// </summary>
        // private Telemetry telemetry;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="port">Port on which to listen</param>
        /// <param name="telemetry">UI window to which the data is sent</param>
        public DataCollector(int port)
        {
            IPEndPoint ip = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
            client = new UdpClient(ip);
            // this.telemetry = telemetry;
        }

        ~DataCollector()
        {
            log.Debug("Closing UDP Stream");
            client.Close();
        }

        /// <summary>
        /// Starts a new Task that opens a port and receives the UDP Packets from the game.
        /// </summary>
        public void StartListen()
        {
            Task.Factory.StartNew(() => Listen(client));
        }

        public static T ConvertToPacket<T>(byte[] bytes)
            where T : struct
        {
            var handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            var stuff = ConvertToPacket<T>(handle.AddrOfPinnedObject());
            handle.Free();
            return stuff;
        }

        public static T ConvertToPacket<T>(IntPtr ptr)
            where T : struct
        {
            return (T)Marshal.PtrToStructure(ptr, typeof(T));
        }

        /// <summary>
        /// Asynchroniously listens for packets from the game and sends them to the UI
        /// </summary>
        /// <param name="client">UDP Client used for listening for packets</param>
        async void Listen(UdpClient client)
        {
            log.Debug("Starting UDP Stream Listening");
            while (true)
            {
                //log.Debug("----------------------------");
                try
                {
                    //log.Debug("Enter Try Block");
                    var result = await client.ReceiveAsync();
                    var bytes = result.Buffer;
                    //log.Debug("Bytes:" + bytes.Length);
                    ExtractHeader(bytes);
                }
                catch (Exception e)
                {
                    log.Fatal(e.ToString());
                    Console.WriteLine(e.ToString());
                }
            }
        }

        private void ExtractHeader(byte[] bytes)
        {
            byte[] tmpHeader = new byte[21];
            for (int i = 0; i < 21; i++)
            {
                tmpHeader[i] = bytes[i];
            }
            PacketHeader header = ConvertToPacket<PacketHeader>(tmpHeader);
            //log.Debug(header.m_packetId);
            SelectPacket(bytes, header.m_packetId);
        }

        private static void SelectPacket(byte[] bytes, byte packetId)
        {
            switch (packetId)
            {
                case 0:
                    PacketMotionData motion = ConvertToPacket<PacketMotionData>(bytes);
                    //log.Info("Received Motion Data");
                    break;
                case 1:
                    PacketSessionData session = ConvertToPacket<PacketSessionData>(bytes);
                    //log.Info("Received Session Data");
                    break;
                case 2:
                    PacketLapData lap = ConvertToPacket<PacketLapData>(bytes);
                    //log.Info("Received Lap Data");
                    break;
                case 3:
                    PacketEventData evnt = ConvertToPacket<PacketEventData>(bytes);
                    //log.Info("Received Event Data");
                    log.Info(evnt.m_eventStringCode.ToString());
                    SelectEventType(evnt);
                    break;
                case 4:
                    PacketParticipantsData part = ConvertToPacket<PacketParticipantsData>(bytes);
                    //log.Info("Received Participant Data");
                    break;
                case 5:
                    PacketCarSetupData setup = ConvertToPacket<PacketCarSetupData>(bytes);
                    //log.Info("Received Setup Data");
                    break;
                case 6:
                    PacketCarTelemetryData tlmtry = ConvertToPacket<PacketCarTelemetryData>(bytes);
                    //log.Info("Received Telemetry Data");
                    break;
                case 7:
                    PacketCarStatusData status = ConvertToPacket<PacketCarStatusData>(bytes);
                    //log.Info("Received Status Data");
                    break;
                default:
                    log.Error("No Package Format found");
                    break;
            }
        }

        private static void SelectEventType(PacketEventData evnt)
        {
            string evntcode = new System.Text.ASCIIEncoding().GetString(evnt.m_eventStringCode);
            switch (evntcode.ToLower()) {
                case "ftlp":
                    log.Info("Fastest Lap");
                    log.Info(evnt.m_eventDetails.ftlp.vehicleIdx);
                    TimeSpan laptime = TimeSpan.FromSeconds(evnt.m_eventDetails.ftlp.lapTime);
                    log.Info(evnt.m_eventDetails.ftlp.lapTime);
                    log.Info(laptime.ToString("mm':'ss'.'fff"));
                    break;
                case "rtmt":
                    log.Info("Retirement");
                    log.Info(evnt.m_eventDetails.rtmt.vehicleIdx);
                    break;
                case "tmpt":
                    log.Info("Team Mate in Pits");
                    log.Info(evnt.m_eventDetails.tmpt.vehicleIdx);
                    break;
                case "rcwn":
                    log.Info("Race Winner");
                    log.Info(evnt.m_eventDetails.rcwn.vehicleIdx);
                    break;
                case "ssta":
                    log.Info("Session Started");
                    break;
                case "send":
                    log.Info("Session Ended");
                    break;
                case "drse":
                    log.Info("Race Control enabled DRS");
                    break;
                case "drsd":
                    log.Info("Race Control Disabled DRS");
                    break;
                case "chqf":
                    log.Info("Checkered Flag has been waved");
                    break;
                default:
                    log.Error("No Event Type Found!");
                    break;
            }
        }
    }
}
